# Astropouet

## Utilisation

Pour utiliser astropouet : 

`python astropouet.py`

Les résultats sont à consulter dans le fichier `output.html`.

On peut préciser que les offres récentes doivent être marquées d'une étoile dans
les résultats (ici jusqu'à J-2) : 

`python astropouet.py -a 2`


## Base sqlite

### Introduction

Astropouet va récolter des offres de postdoc / PhD en astronomie sur différentes
bases : spacecareers, earthworks-jobs, eurosciencejobs, l'AAS...

La base sqlite incluse permet de stocker les hashs des titres des offres deja
observées à l'execution d'astropouet. A chaque hash on associe un timestamp qui 
indique alors la première date à laquelle l'offre à été observée. Ceci permet
in fine de pouvoir indiquer quelles sont les offres publiée il y a moins de N
jour et de les marquer visuellement dans le fichier de résultats.

Cette base héberge aussi les information liées à chaque offre : titre, 
localisation, description, deadline et URL.

### Création de la base

Si elle n'existe pas, la base `joboffers.db` est créée automatiquement au 
lancement de astropouet.

Pour créer la base manuellement, si besoin : 

```
$ sqlite3 hashes.db

sqlite> create table hashes(hash varchar(50), timestamp INTEGER, title 
	varchar(200), location varchar(100), deadline varchar(100), description 
	varchar(500), lien varchar(300));
```

