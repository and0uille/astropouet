#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import argparse 
import logging
import lxml.html
import os
import shutil
from datetime import datetime
import hashlib
import sqlite3
import feedparser
import re
# Pour le warning SSL
from urllib3.exceptions import InsecureRequestWarning


def banner():
    """ Afficher la bannière de début"""
    print('')
    print('*****************************************************')
    print("  ___      _                                    _    ")
    print(" / _ \    | |                                  | |   ")
    print("/ /_\ \___| |_ _ __ ___  _ __   ___  _   _  ___| |_  ")
    print("|  _  / __| __| '__/ _ \| '_ \ / _ \| | | |/ _ \ __| ")
    print("| | | \__ \ |_| | | (_) | |_) | (_) | |_| |  __/ |_  ")
    print("\_| |_/___/\__|_|  \___/| .__/ \___/ \__,_|\___|\__| ")
    print("                        | |                          ")
    print("                        |_|                          ")
    print('*****************************************************')
    print('')

def check_positive(value):
    """ Verifie que le nombre de jours passe en argument est un entier 
    (positif)
    """
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s invalide - entier negatif" % value)
    return ivalue


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def is_new(sqlcon, titre, age, location, deadline, description, lien, source):
    """ Verifie si le hash du titre d'offre telecharge existe deja dans la base
    puis si le hash est recent ou non. On ajuste la valeur du paramètre new en 
    fonction.
    :param sqlcon: handle de connexion de la base sqlite
    :param titre: Titre de l'offre a analyser
    :param age: nombre de jours pendant lequel une offre est considérée comme 
                nouvelle
    :param location: lieu de l'offre de d'emploi
    :param deadline: date limite de condidature pour l'offre
    :param description: description de l'offre d'emploi
    :param lien: URL de l'offre d'emploi
    :param source: Site ayant publié l'offre
    :return: contenu de la cellule "new" en HTML
    """
    new = ""
    logging.info("Hash: SHA1("+titre+")")

    # Initialisation de hashlib avant chaque calcul (important)
    hashme = hashlib.sha1()
    hashme.update(str.encode(titre))
    hashTitre = hashme.hexdigest()

    timestamp = round(datetime.now().timestamp())
    cur = sqlcon.cursor()
    cur.execute("SELECT hash FROM hashes where hash=?", (hashTitre,))

    if cur.fetchall():
        # Le hash existe deja, on regarde le timestamp pour voir si 
        # l'entree est assez recente (3600*24*age)
        rows = cur.execute("SELECT timestamp FROM hashes where hash=?", 
                (hashTitre,))
        
        for row in rows:
            timestamp = row[0]
        # Nombre de secondes en 1j : 60*60*24 = 86400
        delta = round(datetime.now().timestamp() - timestamp)
        if delta <= 86400*age:
            new = "<img src='image/new.png'>"
            logging.info("Nouveau ! Car " + str(delta)+" (" + \
                str(round(datetime.now().timestamp())) + " - " + \
                str(timestamp)+") <= "+str(86400*age))
            logging.info("Hash: "+hashTitre)
            logging.info("Date DB : "+str(row[0]))
    else:
        # Le hash n'existe pas encore en base de données, on l'ajoute
        cur.execute("INSERT into hashes (hash, timestamp, title, location, \
                deadline, description, lien, source) values (?,?,?,?,?,?,?,?)", 
                (hashTitre, timestamp, titre, location, deadline, description, \
                lien, source))
        sqlcon.commit()
        new = "<img src='image/new.png'>"
        logging.info("Nouveau ! Ajout à la base")
        logging.info("Hash: "+hashTitre)
    return new


def get_latest(sqlcon, age):
    """ Extrait de la base les offres les plus récentes.
    :param sqlcon: handle de connexion de la base sqlite
    :param age: nombre de jours pendant lequel une offre est considérée comme
                nouvelle.
    :return: tableau complet au format HTML, pret à intégrer le template.
    """

    logging.info("Creating the 'general' tab")
    tableLines = ""

    timestamp = round(datetime.now().timestamp())
    cur = sqlcon.cursor()
    cur.execute("SELECT timestamp,title,description,location, deadline, lien, \
        source FROM hashes")
    
    for job_offer in cur.fetchall():
        timestamp = job_offer[0]
        job_title = job_offer[1]
        job_desc = job_offer[2]
        job_location = job_offer[3]
        job_deadline = job_offer[4]
        lien = job_offer[5]
        source = job_offer[6]

        delta = round(datetime.now().timestamp() - timestamp)
        if delta <= 86400*age:
            if lien:
                tableLines += tableLine([source, "<a href='" + lien +"'>" + \
                    job_title + "</a>", job_desc,  job_location, job_deadline])
            else:
                tableLines += tableLine([source, job_title, job_desc, \
                    job_location, job_deadline])

    return tableLines


def count_latest(sqlcon, age):
    """ Compte les offres les plus récentes.
    :param sqlcon: handle de connexion de la base sqlite
    :param age: nombre de jours pendant lequel une offre est considérée comme
                nouvelle.
    :return: nombre d'offre enregistrées ces "ages" derniers jours
    """

    now = str(round(datetime.now().timestamp()))
    newest=str(86400*age)
    cur = sqlcon.cursor()
    cur.execute("SELECT COUNT(*) from hashes where "+now+"-timestamp<"+newest)
    nb_newest = cur.fetchone()
    return nb_newest[0]

def tableLine(listeOffre):
    """ Mise en forme des données vers tableau html. Sors une ligne de tableau.
        Entree sous forme de liste (une info par cellule de la ligne)
        :param listeOffre: données d'une offre d'emploi
        :return: Ligne de table HTML
    """
    offreHtml = ''
    offreHtml += '<tr>'
    i = 0

    # On centre la première colonne
    for elem in listeOffre :
        if i == 0: 
            offreHtml += '<td style="text-align:center">'+str(elem)+'</td>'
        else: 
            offreHtml += '<td>'+str(elem)+'</td>'
        i = i + 1
    offreHtml += '</tr>'
    return offreHtml


def writeOutput(pattern, content):
    """ Remplace les commentaires du template par le contenu de chaque tableau 
        d'offre. Une ligne par offre.
    :param pattern: Motif a rechercher dans template.html
    :param content: Ensemble des lignes du tableau au format HTML
    """
    with open(rootpath+'/output.html', "r") as file:
        filedata = file.read()
    filedata = filedata.replace(pattern, content)
    with open(rootpath+'/output.html', "w") as file:
        file.write(filedata)


def dir_path(string):
    """ Vérifier que la chaine donnée est un répertoire existant
    :param string: Chemin à vérifier
    """
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)


def main(args, sqlcon, rootpath):

    # Configuration des options de logs pour le mode debug
    if args.debug == True:
        logging.basicConfig(filename=rootpath+'/debug.log',level=logging.DEBUG)
    
    # On verifie que le template existe et on le copie pour le completer
    if not os.path.exists(rootpath+'/template.html'):
        sys.exit("Le template (template.html) n'existe pas !")
    
    shutil.copyfile(rootpath+'/template.html', rootpath+'/output.html')

    nbNouveau = 0

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("| Spacecareers.uk |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de spacecareers.uk...")

    # Telecharger la page et selectionner la liste d'annonces
    html = requests.get('https://spacecareers.uk/views/jobs/job_search_results.php?no_criteria=0&p=job_search&keyword=&employer_id=&job_duration_min=&job_duration_max=&job_type[]=Fellowship&job_type[]=Graduate%20Position&job_type[]=PhD&job_type[]=Postdoctorate%20Position&job_country[]=Austria&job_country[]=Belgium&job_country[]=Europe%20Wide&job_country[]=France&job_country[]=French%20Guiana&job_country[]=Germany&job_country[]=Italy&job_country[]=Lithuania&job_country[]=Luxembourg&job_country[]=Romania&job_country[]=Spain&job_country[]=The%20Netherlands&job_country[]=UK')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//li')


    # Analyser et filtrer le contenu de chaque annonce
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./div[2]/b/a/text()')[0]
        job_link = job.xpath('./div[2]/b/a/@href')[0]
        job_employer = job.xpath('./div[2]/a/text()')[0]
        job_type = job.xpath('./div[2]/div/span[1]/text()')[0]
        job_city = job.xpath('./div[2]/div/span[2]/span[1]/text()')[0]
        job_country = job.xpath('./div[2]/div/span[2]/span[2]/text()')[0]
        job_duration = job.xpath('./div[2]/div/span[3]/text()')[0]
        job_deadline = job.xpath('./div[2]/div/span[4]/time/text()')

        # On affiche le resultat dans le fichier de debug
        if args.debug == True:
            logging.info("["+job_type.strip()+"] - "+job_title.strip())
            logging.info(job_employer.strip()+" - "+ job_city.strip() + \
                    ", " + job_country.strip())
            logging.info(job_duration.strip()+", deadline: " + \
                    ''.join(job_deadline))
            logging.info("https://spacecareers.uk"+job_link)


        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title.strip(), args.age, job_city.strip()+" - \
                "+job_country.strip(), ''.join(job_deadline), "", 
                "https://spacecareers.uk"+job_link, "Spacecareers")

        if new != "":
            nbNouveau = nbNouveau + 1 

        # On ecrit dans la tableau de resultats
        tableLines += tableLine([new, "<a href='https://spacecareers.uk" +
            job_link+"'>"+job_title.strip()+"</a>", job_city.strip()+" - "+
            job_country.strip(), job_duration.strip()+", deadline: "+
            ''.join(job_deadline)])

    writeOutput("<!---------- SPACECAREERS --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("| Earthworks-jobs |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de earthworks-jobs...")

    html = requests.get('http://www.earthworks-jobs.com/space.htm')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//tr')
    paysInterdits = ['USA', 'Canada', 'Taiwan', 'Japan']
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./td[1]/font/a/b/text()')
        job_link = job.xpath('./td[1]/font/a/@href')
        job_city = job.xpath('./td[2]/font/text()')
        job_country = job.xpath('./td[2]/font/text()')
        job_deadline = job.xpath('./td[4]/font/text()')
        if job_title and job_city[1] not in paysInterdits :
            logging.info(job_title[0])
            logging.info(job_city[0]+" "+job_city[1])
            logging.info("Deadline: "+job_deadline[0])
            
            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title[0], args.age, job_city[0]+" - " + \
                    job_city[1], job_deadline[0], "", job_link[0], 
                    "Earthworks-jobs")

            tableLines += tableLine([new, "<a href='"+job_link[0]+"' </a>"+
                job_title[0], job_city[0]+" - "+job_city[1], job_deadline[0]])
    writeOutput("<!---------- EARTHWORKS --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+---------------------+")
    logging.info("| eurosciencejobs.com |")
    logging.info("+---------------------+\n")

    print("[+] Traitement de eurosciencejobs.com...")

    html = requests.get('https://www.eurosciencejobs.com/job_search/category/space_and_astronomy/job_type/phd_required/job_type/postdoc')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//html/body/div[1]/div[3]/div/div/div[3]/ul/li')

    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./div[1]/div[2]/h3/a/text()')
        job_link = job.xpath('./div[1]/div[2]/h3/a/@href')
        job_employer = job.xpath('./div[1]/div[2]/div[1]/text()')
        job_location = job.xpath('./div[1]/div[2]/div[2]/text()')
        job_deadline = job.xpath('./div[3]/div[1]/div/text()')
        if not job_title:
            job_title = job.xpath('./div[1]/div/div[1]/div[1]/div/h3/a/text()')
            job_link = job.xpath('./div[1]/div/div[1]/div[1]/div/h3/a/@href')
            job_employer = job.xpath('./div[1]/div/div[1]/div[2]/text()')
            job_location = job.xpath('./div[1]/div/div[1]/div[3]/text()')
            job_deadline = job.xpath('./div[2]/div[1]/div/text()')

        if job_title:
            logging.info(job_title[0])
            logging.info(job_employer[0] + ' - ' + job_location[0])
            deadline = job_deadline[0].strip().replace('\n','').replace('    ','')
            logging.info(deadline)

            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title[0], args.age, job_location[0], 
                    deadline, "", 'https://www.eurosciencejobs.com' + \
                    job_link[0], "eurosciencejobs")

            tableLines += tableLine([new, 
                "<a href='https://www.eurosciencejobs.com" + job_link[0] + \
                "' </a>"+job_title[0], job_employer[0] + ' - '+job_location[0],\
                deadline])

    writeOutput("<!---------- EUROSCIENCEJOBS --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+---------------------+")
    logging.info("|         AAS         |")
    logging.info("+---------------------+\n")
    
    print("[+] Traitement de American Astronomical Society...")

    # Filtre : UK, Espagne, Italie, Portugal, France, Irlande
    filtrePays = ["GB", "ES", "IT", "PT", "FR", "IE"]
    html = requests.get('https://jobregister.aas.org/jobs/query?publish_date_start=2020-09-01&publish_date_end=&title=&body=&job_category=Other%2CFacPosNonTen%2CFacPosTen%2CPostDocFellow%2CPreDocGrad%2CSciEng%2CSciMgmt%2CSciTechStaff&institution_classification=Other%2CFrgn%2CGovA%2CInds%2CLA%2CPl%2CRL%2CSM&location_country=GB,ES,IT,PT,FR,IE,&salary_min=0&salary_max=0&hourly_rate_min=0&hourly_rate_max=0&stipdend_min=0&stipdend_max=0')

    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('/html/body/div[2]/div/div[5]/div/div[2]/div/div[2]/div/div/table/tbody')

    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./tr/td[1]/a/text()')
        job_link = job.xpath('./tr/td[1]/a/@href')
        job_employer = job.xpath('./tr/td[2]/text()')
        job_location = job.xpath('./tr/td[3]/text()')
        job_deadline = job.xpath('./tr/td[5]/text()')
        
        for i in range(len(job_title)):
            logging.info(job_title[i])
            logging.info(job_employer[i]+" - "+job_location[i])
            logging.info(job_deadline[i])

            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title[i], args.age, job_location[i], 
                    job_deadline[i], "", "https://jobregister.aas.org" + \
                            job_link[i], "AAS")

            if new != "":
                nbNouveau = nbNouveau + 1 

            tableLines += tableLine([new, "<a href='https://jobregister.aas.org"+
                job_link[i]+"'>"+job_title[i]+"</a>", job_employer[i] + ' - ' + 
                job_location[i], job_deadline[i]])
    writeOutput("<!---------- AAS --------------->", tableLines)


    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+---------------------+")
    logging.info("| AcademicJobsOnline  |")
    logging.info("+---------------------+\n")
    
    print("[+] Traitement de AcademicJobsOnline...")

    # Filtre : Astrophysics
    html_astrophysics = requests.get('https://academicjobsonline.org/ajo?joblist-183-0-0----0-s--')
    doc_astrophysics = lxml.html.fromstring(html_astrophysics.content)
    jobs = doc_astrophysics.findall('.//dl')

    # Filtre : Astronomy
    html_astronomy = requests.get('https://academicjobsonline.org/ajo?joblist-59-0-0----0-s--')
    doc_astronomy = lxml.html.fromstring(html_astronomy.content)
    jobs = jobs + doc_astronomy.findall('.//dl')

    # Filtre : Planetary and Stellar Astrophysics
    html_planet = requests.get('https://academicjobsonline.org/ajo?joblist-566-0-0----0-s--')
    doc_planet = lxml.html.fromstring(html_planet.content)
    jobs = jobs + doc_planet.findall('.//dl')

    # Filtre : Planetary Sciences
    html_plasci = requests.get('https://academicjobsonline.org/ajo?joblist-481-0-0----0-s--')
    doc_plasci = lxml.html.fromstring(html_plasci.content)
    jobs = jobs + doc_plasci.findall('.//dl')

    paysInterdits = ["US", "CA", "CN", "JP"]
    tableLines = ''

    for job in jobs:
        job_title = job.xpath('./dt/ol/li/text()')
        job_link = job.xpath('./dt/ol/li/a/@href')
        job_employer = job.xpath('./dt/b/a[1]/text()')
        job_dpt = job.xpath('./dt/b/a[2]/text()')
        job_location = job.xpath('./dt/ol/li/span[1]/text()')
        job_deadline = 'X'
        
        try:
            job_employer = job_employer[0]
        except:
            job_employer = "-"

        try:
            job_dpt = job_dpt[0]
        except:
            job_dpt = "-"

        if job_title : 
            if not any(pays in "".join(job_location) for pays in paysInterdits):
                logging.info(job_title[1][2:])
                logging.info(job_employer[0]+" - "+job_dpt)

                # On hash le titre et on compare avec la base
                new = is_new(sqlcon, job_title[1][2:], args.age, 
                        job_location[0], "", "", 
                        "https://academicjobsonline.org"+job_link[0], 
                        "AcademicJobOnline")

                if new != "":
                    nbNouveau = nbNouveau + 1 

                tableLines += tableLine([new, 
                    "<a href='https://academicjobsonline.org"+ job_link[0]+"'>"\
                    +job_title[1][2:]+"</a>", job_employer + ' - ' + \
                    job_location[0], "X"])

    writeOutput("<!---------- ACADEMICJOBSONLINE --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|   PhD studies   |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de PhD studies...")

    html = requests.get('https://www.phdstudies.com/Astrophysics/')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="program-listitem relative"]')
    paysInterdits = ['USA', 'Canada', 'Taiwan', 'Japan']
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./div[@class="title"]/a/h4/text()')
        job_link = job.xpath('./div[@class="title"]/a/@href')
        job_employer = job.xpath('./div[@class="school"]/text()')
        job_city = job.xpath('./div[@class="locations"]/span[@class="location"]/text()')
        logging.info(job_title[0])
        logging.info(job_link[0].strip())
        logging.info(job_city[0].strip()+" "+job_employer[0].strip())

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title[0], args.age, job_city[0].strip(), 
                "", "", job_link[0].strip(), "PhD studies")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='"+job_link[0].strip()+"' </a>"+
            job_title[0].strip(), job_city[0].strip(), 'X'])

    writeOutput("<!---------- PHDSTUDIES --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|       OSEA      |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de OSEA...")

    html = requests.get('https://www.sea-astronomia.es/empleos-becas')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('/html/body/div[2]/div/div/div/div[2]/div[2]/div/div/div[3]/div/table/tbody/tr')
    paysInterdits = ['USA', 'Canada', 'Taiwan', 'Japan']
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./td[1]/a/text()')
        job_link = job.xpath('./td[1]/a/@href')
        job_type = job.xpath('./td[2]/text()')
        job_deadline = job.xpath('./td[3]/span/text()')
        job_employer = job.xpath('./td[5]/text()')
        job_country = job.xpath('./td[6]/text()')
        job_description = job.xpath('./td[7]/text()')
        logging.info(job_title[0])
        logging.info(job_link[0].strip())
        logging.info(job_country[0].strip()+" "+job_employer[0].strip())
        logging.info(job_deadline[0])

        # On garde le debut de la description (peut être longue)
        job_description = "".join(job_description).strip()
        if len(job_description) > 200:
            job_description = job_description[:200]+"..."

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title[0], args.age, job_country[0].strip(), 
                job_deadline[0], job_description, 
                'https://www.sea-astronomia.es'+job_link[0].strip(), "OSEA")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='https://www.sea-astronomia.es"+
            job_link[0].strip()+"' </a>"+ job_title[0].strip(), \
                job_country[0].strip(), job_deadline[0], job_description])

    writeOutput("<!---------- OSEA --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|       EAS       |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de European Astronomical Society...")


    # Encoding : ISO-8859-1
    html = requests.get('https://eas.unige.ch/jobs.jsp', verify=False)
    doc = lxml.html.fromstring(html.content.decode('iso-8859-1').encode('utf8'))
    jobs = doc.xpath('//table[@class="job"]')
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./tr[1]/td[1]/text()')
        job_deadline = job.xpath('./tr[1]/td[2]/span[1]/text()')
        job_description = job.xpath('./tr[2]/td/descendant-or-self::*/text()')
        description = ''.join(job_description).strip().replace('\r\n', "")

        if len(job_title) != 0:
            logging.info(job_title[0])
            logging.info(job_deadline[0])

            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title[0], args.age, "", 
                    job_deadline[0].strip(), description, "", "EAS")

            if new != "":
                nbNouveau = nbNouveau + 1 

            tableLines += tableLine([new, job_title[0], job_deadline[0].strip(), 
                description])
        else:
            logging.info("Empty job title, see https://eas.unige.ch/jobs.jsp")

    writeOutput("<!---------- EAS --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|  RIASTRONOMIA   |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de Riastronomia...")

    # Riastronomia filters python requests User Agent
    headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) \
            Gecko/20100101 Firefox/87.0"}

    html = requests.get('https://riastronomia.es/en/contratos-postdoctorales-2/',\
            verify=False, headers=headers)
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="fusion-column-wrapper fusion-flex-column-wrapper-legacy"]/div[starts-with(@class, "fusion-text")]')
    tableLines = ''
    interdits = ['Toronto', 'California', 'USA']
    for job in jobs:
        job_title = job.xpath('./div/div/div/ul/li[1]/descendant-or-self::*/text()')
        job_employer = job.xpath('./div/div/div/ul/li[2]/text()')
        job_deadline = job.xpath('./div/div/div/ul/li[3]/strong/text()')
        job_link = job.xpath('./div/div/div/ul/li[4]/a/@href')
        if job_title:
            title = ''.join(job_title)
            deadline = "Erreur"
            link= "Erreur"
            if job_deadline:
                deadline = job_deadline[0].strip()
            if job_link:
                link = job_link[0].strip()

            if not any(loc in "".join(job_employer) for loc in interdits):
                logging.info(title)
                logging.info(job_employer[0])
                logging.info(deadline)

                # On hash le titre et on compare avec la base
                new = is_new(sqlcon, title, args.age, job_employer[0].strip(), 
                        deadline, "", link, "Riastronomia")

                if new != "":
                    nbNouveau = nbNouveau + 1 

                tableLines += tableLine([new, "<a href='"+link+"' </a>"+ title, 
                    job_employer[0].strip(), deadline])

    writeOutput("<!---------- RIASTRONOMIA --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|    hyperspace   |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de Hyperspace...")

    html = requests.get('https://hyperspace.uni-frankfurt.de/category/jobs/')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//article')
    paysInterdits = ['USA', 'Canada', 'Taiwan', 'Japan', 'China']
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./header/h1/a/text()')
        job_link = job.xpath('./header/h1/a/@href')
        job_description = job.xpath('./div/p/text()')

        # Le titre comprend le lieu, on l'extrait
        job_location = job_title[0].split(', ', 1)[1]
        job_title = job_title[0].split(', ', 1)[0]

        if not any(pays in "".join(job_location) for pays in paysInterdits):
            logging.info(job_title)
            logging.info(job_location)
            logging.info(job_deadline)

            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title, args.age, job_location, "",
                    job_description[0], job_link[0], "Hyperspace")

            if new != "":
                nbNouveau = nbNouveau + 1 

            tableLines += tableLine([new, "<a href='"+job_link[0]+"'>"+ \
                job_title+"</a>", job_location, job_description[0]])

    writeOutput("<!---------- HYPERSPACE --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|   Cosmocoffee   |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de Cosmocoffee...")

    html = requests.get('https://cosmocoffee.info/viewforum.php?f=8', verify=False)
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//ul[@class="topiclist topics"]/li')
    tableLines = ''
    # On ne prend que les 11 premiers
    ctr = 0
    for job in jobs:
        if ctr <= 10: 
            job_title = job.xpath('./dl/dt/div/a/text()')
            job_link = job.xpath('./dl/dt/div/a/@href')
            job_posted = job.xpath('./dl/dd[3]/span/text()')
            posted = "".join(job_posted).replace('by', '').strip()

            # Le titre comprend le lieu, on l'extrait
            job_location = ""
            try:
                job_location = job_title[0].split('at ', 1)[1]
                job_title = job_title[0].split('at ', 1)[0]
            except:
                job_title = job_title[0]
                job_location = ""

            logging.info(job_title)
            logging.info(job_link)
            logging.info(job_location)
            logging.info(posted.strip())

            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title, args.age, job_location, "", "", \
                    'https://cosmocoffee.info/'+job_link[0], "Cosmocoffee")

            if new != "":
                nbNouveau = nbNouveau + 1 

            tableLines += tableLine([new, "<a href='https://cosmocoffee.info/"+
                job_link[0]+"'>"+ job_title+"</a>", job_location, "Posted: "+ \
                posted])
            ctr = ctr + 1

    writeOutput("<!---------- COSMOCOFFEE --------------->", tableLines)

   # -------------------------------------------------------------------------#
   # -------------------------------------------------------------------------#

    logging.info("+---------------------+")
    logging.info("| Universitypositions |")
    logging.info("+---------------------+\n")

    print("[+] Traitement de University Positions...")

    html = requests.get('https://www.universitypositions.eu/jobs/category/303,342', verify=False)
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="job-list-wrap"]/div')
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./div/div[2]/div/div/h2/a/text()')
        job_link = job.xpath('./div/div[2]/div/div/h2/a/@href')
        job_location = job.xpath('./div/div[2]/div/div/div/span/strong/text()')
        job_posted = job.xpath('./div/div[2]/div/div/span/text()')

        logging.info(job_title)
        logging.info(job_link)
        logging.info(job_location)
        logging.info(job_posted)

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title[0], args.age, job_location[0], "", "", \
                job_link[0], "Universitypositions")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href="+ job_link[0]+">" + \
            job_title[0]+"</a>", job_location[0], job_posted[0]])

    writeOutput("<!---------- UNIVERSITYPOSITIONS --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+---------------------+")
    logging.info("|  Academic Positions |")
    logging.info("+---------------------+\n")

    print("[+] Traitement de Academic Positions...")
    
    feed = feedparser.parse('https://academicpositions.com/rss/?positions[0]=Postdoc&positions[1]=PhD&mainFields[0]=Space%20Science&convertParams=true&index=jobs:en:1')
    tableLines = ''
    for i in range(len(feed.entries)):
        job_title = feed.entries[i]['title']
        job_link = feed.entries[i]['link']
        job_posted = feed.entries[i]['published']

        logging.info(job_title)
        logging.info(job_link)
        logging.info(job_posted)

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title[0], args.age, "", "", "", job_link, 
                "Academic Positions")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href="+ job_link+">" + \
            job_title+"</a>", job_posted])

    writeOutput("<!---------- ACADEMICPOSITIONS --------------->", tableLines)


    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|       INAF      |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de INAF...")

    html = requests.get('http://www.inaf.it/it/lavora-con-noi/assegni-di-ricerca/assegni-di-ricerca/')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="singlebando"]')
    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./h2[@class="bandoTitle"]/a/text()')
        job_link = job.xpath('./h2[@class="bandoTitle"]/a/@href')
        job_city = job.xpath('./div/p[@class="bandoDescription"]/text()')
        job_deadline = job.xpath('./div/p[@class="bandoDates"]/span/text()')

        if len(job_city) == 0:
            job_city = ['Unknown']

        logging.info(job_title[0])
        logging.info(job_link[0].strip())
        logging.info(job_city[0].strip())

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title[0], args.age, job_city[0].strip(), 
                job_deadline[1], "", job_link[0].strip(), "INAF")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='"+job_link[0].strip()+"' </a>"+
            job_title[0].strip(), job_city[0].strip(), job_deadline[1]])

    writeOutput("<!---------- INAF --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|    FINDAPHD     |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de Find a PHD...")

    html = requests.get('https://www.findaphd.com/phds/astrophysics/?30gk2B1&Show=M')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="resultsRow phd-result-row-standard phd-result col-xs-24 tight"]')
    tableLines = ''
    for job in jobs:
        job_title = ['']
        job_link = ['']
        job_city = ['']

        if job.xpath('./div[2]/a/text()'):
            job_title = job.xpath('./div[2]/a/text()')
            job_link = "https://www.findaphd.com"+job.xpath('./div[2]/a/@href')[0]
        if job.xpath('./div[2]/div[1]/a/span/text()'):
            job_city = job.xpath('./div[2]/div[1]/a/span/text()')

        logging.info(job_title[0])
        logging.info(job_link[0].strip())
        logging.info(job_city[0].strip())

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title[0], args.age, job_city[0].strip(), "", 
                "", job_link.strip(), "FindAPHD")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='"+job_link.strip()+"' </a>"+
            job_title[0].strip(), job_city[0].strip() ])

    writeOutput("<!---------- FINDAPHD --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("| Mailing PHAROS  |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de la mailing list PHAROS...")

    # On récupere les infos du mois le plus récent
    html = requests.get('https://www.ice.csic.es/pipermail/pharos/')
    doc = lxml.html.fromstring(html.content)
    job_links = "https://www.ice.csic.es/pipermail/pharos/" + \
                    doc.xpath('//table/tr[2]/td[2]/a[2]/@href')[0]
    logging.info(job_links)

    # On liste les offres publiees ce mois ci
    html2 = requests.get(job_links)
    doc2 = lxml.html.fromstring(html2.content.decode('utf8'))
    jobs = doc2.xpath('/html/body/ul[2]/li')

    tableLines = ''
    for job in jobs:
        job_title = job.xpath('./a[1]/text()')
        job_link = job.xpath('./a[1]/@href')

        if len(job_title) > 0:
            link = job_links.replace('subject.html', '')+"/"+job_link[0]
            logging.info(job_title[0])
            logging.info(link)

            # On hash le titre et on compare avec la base
            new = is_new(sqlcon, job_title[0], args.age, "", "", "", link, 
                    "PHAROS")

            if new != "":
                nbNouveau = nbNouveau + 1 

            tableLines += tableLine([new, "<a href='"+link+"' </a>"+
                job_title[0].strip() ])

    writeOutput("<!---------- MAIL PHAROS --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+---------------------+")
    logging.info("|         PNP         |")
    logging.info("+---------------------+\n")

    print("[+] Traitement de Programme National de Planétologie...")

    html = requests.get('https://pnp-insu.fr/news', verify=False)
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="blogmagazine"]/div')
    tableLines = ''
    
    list_pos = re.findall(r'[0-9]{1,2}\) [A-Z0-9][A-Z0-9a-z\|:,à\'’« »éè\-"]+', html.text)
    
    for i in range(0, 8):

        job_title = list_pos[i]
        job_link = "https://pnp-insu.fr/news"

        logging.info(job_title)
        logging.info(job_link)

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title, args.age, "", "", "", job_link, "PNP")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href="+ job_link+">" + \
            job_title+"</a>"])

    writeOutput("<!---------- PNP --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|  EXOBIOLOGIE    |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de Exobiologie...")

    html = requests.get('https://www.exobiologie.fr/exobiologie/actualites/stages-et-emplois/')
    doc = lxml.html.fromstring(html.content)
    jobs = doc.xpath('//div[@class="galleryitem"]')
    tableLines = ''
    for job in jobs:

        job_title = job.xpath('./h3/a/text()')[0]
        job_link = job.xpath('./h3/a/@href')[0]
        job_description = job.xpath('./p/text()')[0]

        logging.info(job_title)
        logging.info(job_link.strip())
        logging.info(job_description.strip())

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title, args.age, "", "", 
                job_description.strip(), job_link.strip(), "Exobiologie")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='"+job_link.strip()+"' </a>"+
            job_title.strip(), job_description.strip() ])

    writeOutput("<!---------- EXOBIOLOGIE --------------->", tableLines)

    # -------------------------------------------------------------------------#
    # -------------------------------------------------------------------------#

    logging.info("+-----------------+")
    logging.info("|   Europlanet    |")
    logging.info("+-----------------+\n")

    print("[+] Traitement de Europlanet (postdoc)...")

    html = requests.get('http://www.europlanet.tfai.vu.lt/homepage/early-careers-job-opportunities/')
    doc = lxml.html.fromstring(html.content.decode("utf8"))
    jobs = doc.xpath('//div[@class="entry-content"]/h3')
    tableLines = ''
    for job in jobs:
        job_title = ""
        if "".join(job.xpath('.//descendant::*/text()')) == "":
            job_title = "".join(job.xpath('./text()'))
        else:
            job_title = "".join(job.xpath('.//descendant::*/text()'))
        job_desc = " ".join(job.xpath('./../p[preceding-sibling::h3="' + \
                job_title+'"]/text()'))[:350] + "..."
        job_link = "http://www.europlanet.tfai.vu.lt/homepage/early-careers-job-opportunities/"
        logging.info(job_title)
        logging.info(job_link.strip())
        logging.info(job_desc.strip())

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title, args.age, "", "", job_desc.strip(), 
                job_link.strip(), "Europlanet")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='"+job_link.strip()+"' </a>"+
            job_title.strip(), job_desc.strip()])
    
    print("[+] Traitement de Europlanet (PhD)...")

    html = requests.get('http://www.europlanet.tfai.vu.lt/homepage/phd-positions/')
    doc = lxml.html.fromstring(html.content.decode("utf8"))
    jobs = doc.xpath('//div[@class="entry-content"]/h3')
    for job in jobs:
        if len(job.xpath('./text()')) > 0:
            job_title = job.xpath('./text()')[0]
        else:
            job_title = ""
        job_desc = " ".join(job.xpath('./../p[preceding-sibling::h3="' + \
                job_title+'"]/text()'))[:350] + "..."
        job_link = "http://www.europlanet.tfai.vu.lt/homepage/phd-positions/"
        logging.info(job_title)
        logging.info(job_link.strip())
        logging.info(job_desc.strip())

        # On hash le titre et on compare avec la base
        new = is_new(sqlcon, job_title, args.age, "", "", job_desc.strip(), 
                job_link.strip(), "Europlanet")

        if new != "":
            nbNouveau = nbNouveau + 1 

        tableLines += tableLine([new, "<a href='"+job_link.strip()+"' </a>"+
            job_title.strip(), job_desc.strip()])

    writeOutput("<!---------- EUROPLANET --------------->", tableLines)

    # -------------------------------------------------------------------------#

    #######################
    # Lastest jobs offers #
    #######################

    print("[+] Traitement des dernières offres...")
    latest_jobs = get_latest(sqlcon, args.age)
    writeOutput("<!---------- GENERAL --------------->", latest_jobs)


    # -------------------------------------------------------------------------#


    # Ajouter la date et l'heure en en-tete
    now = datetime.now()
    writeOutput("-----TIME AND DATE-----", now.strftime("%a, %d %b %Y %H:%M:%S"))

    # Ajouter la date et l'heure en en-tete
    nb_latest = count_latest(sqlcon, args.age)
    writeOutput("--NB LATEST--", str(nb_latest))

    
    print("[+] Pouet !")
    print('')
    print('  ===> '+str(nbNouveau)+" nouvelles offres !")

if __name__ == "__main__":

    rootpath = "."
    DB_FILENAME = "joboffers.db"

    # Récupérer les arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', dest='debug', default=False, 
        action='store_true', help='Activer le mode debug (debug.log)')
    parser.add_argument('-a', '--age', dest="age", default=5, 
        type=check_positive, help='Tag les annonces des N derniers jours')
    parser.add_argument('-p', '--path', default=".", type=dir_path, 
        help='Chemin du script. Ex: /home/dvador/astropouet/')
    args = parser.parse_args()

    # Pas de warning (SSL)
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

    banner()
    rootpath = args.path

    # Creation de la base de donnees si necessaire
    database = rootpath+"/"+DB_FILENAME
    if not os.path.exists(database):
        print("[+] La base de données n'existe pas, création...")
        conn = sqlite3.connect(database)
        c = conn.cursor()
        c.execute('''create table hashes(hash varchar(50), timestamp INTEGER, 
            title varchar(200), location varchar(100), deadline varchar(100), 
            description varchar(500), lien varchar(300), 
            source varchar(100))''')
        conn.commit()

    # Connexion à la base SQL
    sqlcon = create_connection(rootpath+"/"+DB_FILENAME)

    main(args, sqlcon, rootpath)

    # Déconnexion de la base
    sqlcon.close()

