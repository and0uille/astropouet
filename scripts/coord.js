var villes = {
	'<b>Padova</b><br>INAF<br>Machine learning appliqué à la planetologie': {'lat': '45.40196', 'lon': '11.86885'},
	'<b>Toulouse</b><br>IRAP<br>Sujet de planetologie': {'lat': '43.56246', 'lon': '1.47649'}, 
	'<b>Manchester</b><br>University of Manchester<br>Mineral records of magma storage and crystallization on the Moon': {'lat': '53.46929', 'lon': '-2.23191'},
	'<b>Rome</b><br>INAF<br>Study  of  Jupiter  auroral  regions  by  means  of  the  Juno/JIRAM  data  and  in  view  of JUICE/MAJIS observations': {'lat': '41.8119', 'lon': '12.70491'}
};
